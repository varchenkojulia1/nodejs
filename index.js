const {response} = require('express')
const express = require('express')
const fs = require('fs')
const path = require('path')
const fileNamePattern = new RegExp(/^[A-z1-9.]+\.(txt|log|json|xml|js|yaml)$/)
const app = express()

app.use(express.json());

app.use(express.static(path.join(__dirname, 'web')));

app.get('/', (req, res) => {

    console.log(req.url)
    console.log(req.method)
    res.sendFile(path.resolve('./web/index.html'))

})

app.get('/api/files', (req, res) => {

        try {

            console.log(req.url)
            console.log(req.method)

            fs.readdir(__dirname + '/apiFiles/',
                (err, files) => {
                    if (typeof files === 'undefined') {
                        files = []
                    }
                    err ? console.log(err) :
                        res.status(200).json({"message": "Success", files})
                })

        } catch (err) {

            res.status(500).json({"message": "Server error"})
        }
    }
)

app.get('/api/files/:filename', (req, res) => {

    console.log(req.url)
    console.log(req.method)
    const filename = req.params.filename

    fs.stat(`./apiFiles/${filename}`, (err, stat) => {

        if (err) {

            res.status(400).json({"message": `No file with '${filename}' filename found ${err}`})

        } else if (!fileNamePattern.test(filename)) {

            res.status(400).json({"message": `Incorrect filename format:${filename}`})

        } else {
            const extension = path.extname(`./apiFiles/${filename}`);

            fs.readFile(`./apiFiles/${filename}`, 'utf8', (err, data) => {
                if (err) console.log(err)
                res.status(200).json({
                    "message": "Success",
                    "filename": filename,
                    "content": data,
                    "extension": extension.slice(1, extension.length),
                    "uploadedDate": stat.birthtime
                })
            })
        }
    })
})

app.post('/api/files', (req, res) => {

    if (!fs.existsSync('./apiFiles')) {
        fs.mkdirSync('./apiFiles')
    }
    if (typeof req.body.content === "undefined") {

        res.status(400).json({"message": "Please specify 'content' parameter"})

    } else if (!fileNamePattern.test(req.body.filename)) {

        res.status(400).json({"message": "Incorrect filename format"})

    } else {

        fs.writeFile(path.resolve(`./apiFiles/${req.body.filename}`),
            req.body.content,
            (err) => {
                err ? console.log(err) : null;
            });

        res.status(200).json({
            "message": "File created successfully",
            "status": 200
        })
    }
})

app.listen(8080)
