const createBtn = document.getElementById('createBtn'),
    createInput = document.getElementById('create'),
    createContent = document.getElementById('createContent'),
    fileNamePattern = new RegExp(/^[A-z1-9.]+\.(txt|log|json|xml|js|yaml)$/),
    showListBtn = document.getElementById('showList'),
    showInfoInput = document.getElementById('showInfo'),
    showInfoBtn = document.getElementById('showInfoBtn');

const createInputValidation = (e) => fileNamePattern.test(e.target.value) ? createBtn.removeAttribute('disabled') :
    createBtn.setAttribute('disabled', 'true')

const showInfoValidator = (e) => fileNamePattern.test(e.target.value) ? showInfoBtn.removeAttribute('disabled') :
    showInfoBtn.setAttribute('disabled', 'true')

const createFn = () => {

    let bodyReq = {
        "filename": createInput.value,
        "content": createContent.value
    }

    fetch('http://localhost:8080/api/files', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(bodyReq)
    })
        .then(response => response.json())
        .then(data => alert(data.message))
        .catch(err => alert(err))
        
        createInput.value = '';
        createContent.value = '';
}


const showList = () => {

    fetch('http://localhost:8080/api/files', {
        method: `GET`
    })
        .then(response => response.json())
        .then(data => drawOutput(document.getElementById('outputList'), data.files))
}

const drawOutput = (target, data) => {

    target.innerHTML = '';
    data.forEach(elem => {
        let listItem = document.createElement('li');
        listItem.innerText = elem;
        target.append(listItem)
    })
}

const getInfo = () => {

    fetch(`http://localhost:8080/api/files/${showInfoInput.value}`, {
        method: `GET`
    })
        .then(response => response.json())
        .then(data => {
            document.getElementById('showInfoOutput').innerText = JSON.stringify(data)
        })
}

createInput.addEventListener('input', createInputValidation);
createBtn.addEventListener('click', createFn);
showListBtn.addEventListener('click', showList);
showInfoInput.addEventListener('input', showInfoValidator);
showInfoBtn.addEventListener('click', getInfo);